<?php
/**
 * Plugin Name: Webinar CPTS Plugin
 * Description: This is the Custom Post Type for FRN Webinars.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_webinars_cpts() {

	$labels = array(
		'name'                  => _x( 'Webinars', 'Post Type General Name', 'sl_webinars_cpts' ),
		'singular_name'         => _x( 'Webinar', 'Post Type Singular Name', 'sl_webinars_cpts' ),
		'menu_name'             => __( 'Webinars', 'sl_webinars_cpts' ),
		'name_admin_bar'        => __( 'Webinar', 'sl_webinars_cpts' ),
		'archives'              => __( 'Webinar Archives', 'sl_webinars_cpts' ),
		'attributes'            => __( 'Webinar Attributes', 'sl_webinars_cpts' ),
		'parent_item_colon'     => __( 'Parent Webinar:', 'sl_webinars_cpts' ),
		'all_items'             => __( 'All Webinars', 'sl_webinars_cpts' ),
		'add_new_item'          => __( 'Add New Webinar', 'sl_webinars_cpts' ),
		'add_new'               => __( 'Add New', 'sl_webinars_cpts' ),
		'new_item'              => __( 'New Webinar', 'sl_webinars_cpts' ),
		'edit_item'             => __( 'Edit Webinar', 'sl_webinars_cpts' ),
		'update_item'           => __( 'Update Webinar', 'sl_webinars_cpts' ),
		'view_item'             => __( 'View Webinar', 'sl_webinars_cpts' ),
		'view_items'            => __( 'View Webinars', 'sl_webinars_cpts' ),
		'search_items'          => __( 'Search Webinars', 'sl_webinars_cpts' ),
		'not_found'             => __( 'Not found', 'sl_webinars_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_webinars_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_webinars_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_webinars_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_webinars_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_webinars_cpts' ),
		'insert_into_item'      => __( 'Insert into Webinars', 'sl_webinars_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Webinars', 'sl_webinars_cpts' ),
		'items_list'            => __( 'Webinars list', 'sl_webinars_cpts' ),
		'items_list_navigation' => __( 'Webinars list navigation', 'sl_webinars_cpts' ),
		'filter_items_list'     => __( 'Filter Webinars list', 'sl_webinars_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Webinars', 'sl_webinars_cpts' ),
		'description'           => __( 'Custom Post Type for FRN Webinars', 'sl_webinars_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'for-professionals/educational-webinar-series','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-laptop',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_webinars_cpts', $args );

}
add_action( 'init', 'sl_webinars_cpts', 0 );