<?php
/**
 * Plugin Name: Events CPTS Plugin
 * Description: This is the Custom Post Type for FRN Events.
 * Author: M.M.
 * License: GPL2
*/
 
// Register Custom Post Type
function sl_event_cpts() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'sl_event_cpts' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'sl_event_cpts' ),
		'menu_name'             => __( 'Events', 'sl_event_cpts' ),
		'name_admin_bar'        => __( 'Events', 'sl_event_cpts' ),
		'archives'              => __( 'Event Archives', 'sl_event_cpts' ),
		'attributes'            => __( 'Event Attributes', 'sl_event_cpts' ),
		'parent_item_colon'     => __( 'Parent Event:', 'sl_event_cpts' ),
		'all_items'             => __( 'All Events', 'sl_event_cpts' ),
		'add_new_item'          => __( 'Add New Event', 'sl_event_cpts' ),
		'add_new'               => __( 'Add New', 'sl_event_cpts' ),
		'new_item'              => __( 'New Event', 'sl_event_cpts' ),
		'edit_item'             => __( 'Edit Event', 'sl_event_cpts' ),
		'update_item'           => __( 'Update Event', 'sl_event_cpts' ),
		'view_item'             => __( 'View Event', 'sl_event_cpts' ),
		'view_items'            => __( 'View Events', 'sl_event_cpts' ),
		'search_items'          => __( 'Search Event', 'sl_event_cpts' ),
		'not_found'             => __( 'Not found', 'sl_event_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_event_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_event_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_event_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_event_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_event_cpts' ),
		'insert_into_item'      => __( 'Insert into Event', 'sl_event_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event', 'sl_event_cpts' ),
		'items_list'            => __( 'Events list', 'sl_event_cpts' ),
		'items_list_navigation' => __( 'Events list navigation', 'sl_event_cpts' ),
		'filter_items_list'     => __( 'Filter Events list', 'sl_event_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'sl_event_cpts' ),
		'description'           => __( 'Custom Post Type for FRN Events', 'sl_event_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'conferences','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_event_cpts', $args );

}
add_action( 'init', 'sl_event_cpts', 0 );
?>
