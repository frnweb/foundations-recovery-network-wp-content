<?php // FRN LiveAgent - Settings Callbacks

// disable direct file access
if ( ! defined( 'ABSPATH' ) ) {

	exit;

}


// callback: shortcode section
function frn_liveagent_callback_section_shortcode() {
	echo '<h2><b>[frn_liveagent]</b></h2>';
}



// callback: admin section
function frn_liveagent_callback_section_admin() {

	echo '<p>These settings enable you to customize the WP Admin Area.</p>';

}

// callback: admin section
function frn_webtolead_callback_section_shortcode() {

	echo '<p>These links point the Email Us! button to the Web to Lead form/Thank you Page.</p>';
	
}

// callback: text field
function frn_liveagent_callback_field_text( $args ) {

	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	$value = isset( $options[$id] ) ? sanitize_text_field( $options[$id] ) : '';

	echo '<input id="frn_liveagent_options_'. $id .'" name="frn_liveagent_options['. $id .']" type="text" size="40" value="'. $value .'"><br />';
	echo '<label for="frn_liveagent_options_'. $id .'">'. $label .'</label>';
}

// callback: checkbox field
function frn_liveagent_callback_field_checkbox( $args ) {

	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	$checked = isset( $options[$id] ) ? checked( $options[$id], 1, false ) : '';

	echo '<input id="frn_liveagent_options_'. $id .'" name="frn_liveagent_options['. $id .']" type="checkbox" value="1"'. $checked .'> ';
	echo '<label for="frn_liveagent_options_'. $id .'">'. $label .'</label>';
}

// select field options
function frn_liveagent_options_select() {
	
	return array(
		
		'default'   => 'Default',
		'black_bear_rehab'     => 'Black Bear Rehab',
		'frn'      => 'Foundations Recovery Network',
		'talbott'    => 'Talbott',
		'michaels_house' => 'Michaels House',
		'the_oaks'  => 'The Oaks Treatment',
		'dual_diagnosis'     => 'Dual Diagnosis',
		'the_canyon_malibu'   => 'The Canyon - Malibu',
		'sober_hero'   => 'Sober Hero',
		'skywood_recovery'   => 'Skywood Recovery',
		'rehab_and_treatment'   => 'Rehab and Treatment',
		
	);
	
}

// callback: select field
function frn_liveagent_callback_field_select( $args ) {

	$options = get_option( 'frn_liveagent_options', frn_liveagent_options_default() );

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	$selected_option = isset( $options[$id] ) ? sanitize_text_field( $options[$id] ) : '';

	$select_options = frn_liveagent_options_select();

	echo '<select id="frn_liveagent_options_'. $id .'" name="frn_liveagent_options['. $id .']">';

	foreach ( $select_options as $value => $option ) {

		$selected = selected( $selected_option === $value, true, false );

		echo '<option value="'. $value .'"'. $selected .'>'. $option .'</option>';

	}

	echo '</select> <label for="frn_liveagent_options_'. $id .'">'. $label .'</label>';

}


