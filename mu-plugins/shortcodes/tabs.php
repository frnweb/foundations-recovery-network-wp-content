<?php
// TABS Dependent on ZURB Foundation 6
	function sl_tabs( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> ''
		), $atts );
		static $i = 0;
		$i++;
		
		return '<div class="sl_tabs__container"><ul class="tabs sl_tabs ' . esc_attr($specs['class'] ) . '" data-tabs id="tabs' . $i . '">' . do_shortcode ( $content ) . '</ul></div>';
	}

	add_shortcode ('tabs', 'sl_tabs' );
///TABS

// TAB TITLE
	function sl_tabs_title ( $atts, $content = null ) {

	    $specs = shortcode_atts( array(
	        'class'     => ''
	    ), $atts );

	    static $i = 0;
	    if ( $i == 0 ) {
	        $class = 'is-active';
	    } else {
	        $class = NULL;
	    }
	    $i++;
	    $value = '<li class="tabs-title sl_tabs__title ' . $class . ' '.esc_attr($specs['class'] ).'"><a href="#tabpanel' . $i . '">' .
	    $content . '</a></li>';

	    return $value;
	}

	add_shortcode ('tab-title', 'sl_tabs_title' );
///TAB TITLE

// TAB CONTENT
	function sl_tabs_content( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> ''
		), $atts );
		static $i = 0;
		$i++;
		return '<div class="tabs-content sl_tabs__content ' . esc_attr($specs['class'] ) . '" data-tabs-content="tabs' . $i . '">' . do_shortcode ( $content ) . '</div>';
	}
	add_shortcode ('tab-content', 'sl_tabs_content' );
///TAB CONTENT

// TAB PANEL
	function sl_tabs_panel ($atts, $content = null ) {
		static $i = 0;
	    if ( $i == 0 ) {
	        $class = 'is-active';
	    } else {
	        $class = NULL;
	    }
	    $i++;
	    $content = wpautop(trim($content));
		return '<div class="tabs-panel sl_tabs__panel ' . $class .'" id="tabpanel' . $i . '">' . do_shortcode( $content ) . '</div>';
	}
	add_shortcode ('tab-panel', 'sl_tabs_panel' );
///TAB PANEL
?>