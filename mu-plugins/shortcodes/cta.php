<?php
// CTA
	function sl_cta($atts) {
		$specs = shortcode_atts( array(
			'class'	=> '', 
			), $atts );

		return '<div class="sl_callout sl_callout--cta sl_callout--' . esc_attr($specs['class'] ) .'">
					<h2>Give us a call' .  do_shortcode('[frn_phone ga_phone_location="Phone Clicks in CTA Shortcode" class="sl_button sl_button--phone"]') . '</h2>
					<div class="sl_button-group">
					' . do_shortcode('[lhn_inpage button="chat" text="Chat" offline_text="remove" class="sl_button sl_button--secondary"]') . '
					' . do_shortcode('[lhn_inpage button="email" text="Email" class="sl_button sl_button--secondary"]') . '
					</div>
					<a class="sl_cta__modal" data-open="cta-video">What should I expect when I call?</a>
				</div>
				<div id="cta-video" class="reveal sl_reveal sl_reveal--video" data-reveal data-reset-on-close>
					<div class="responsive-embed widescreen">
					' . do_shortcode('[embed src="https://www.youtube.com/embed/q9JXotss9MA"]') .  '
					</div>
					<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
				</div>';

	}
	add_shortcode( 'cta', 'sl_cta' );
///CTA
?>