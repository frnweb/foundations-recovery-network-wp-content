<?php
// CONTAINER
	function sl_content_locker ( $atts, $content = null ) {
		$page_title = get_the_title();
	    $specs = shortcode_atts( array(
	        'heading'     => 'Subscribe to Unlock Content',
	        'campaign'     => '',
	        'page-title'  =>  $page_title
			), $atts );

	    ob_start ();
		?>
		<div id="mc_embed_signup">
			<form action="https://foundationsrecoverynetwork.us5.list-manage.com/subscribe/post?u=a517fa2b2970f6fb110a5a1dc&amp;id=3675106f6e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" novalidate>
				<div class="sl_form sl_form--email" id="mc_embed_signup_scroll">
					<input type="email" value="" name="EMAIL" class="sl_input sl_form__input" id="mce-EMAIL" placeholder="email address" required>
					<div style="position: absolute; left: -5000px;" aria-hidden="true">
						<input type="text" name="b_a517fa2b2970f6fb110a5a1dc_3675106f6e" tabindex="-1" value="">
					</div>

					<!-- Passing Campaign Name -->
					<input style="display: none;" type="text" value="ContentGate_<?php echo esc_attr($specs['campaign']) ?>" name="MCMPGN" class="" id="mce-MCMPGN">

					<!-- Passing Source URL -->
					<input style="display: none;" type="text" value="<?php echo get_permalink() ?>" name="URL" class="" id="mce-URL">

					<!-- Passing Subscription Page Title -->
					<input style="display: none;" type="text" value="<?php echo esc_attr($specs['page-title']) ?>" name="TITLE" class="" id="mce-TITLE">

					<!-- Passing Who Interest Group -->
					<div style="display: none;" class="mc-field-group input-group">
						<input type="checkbox" value="1" name="group[22101][1]" id="mce-group[22101]-22101-0"><label for="mce-group[22101]-22101-0">I am seeking information for myself</label>
						<input type="checkbox" value="2" name="group[22101][2]" id="mce-group[22101]-22101-1"><label for="mce-group[22101]-22101-1">I am seeking information for a loved one</label>
						<input type="checkbox" value="4" name="group[22101][4]" id="mce-group[22101]-22101-2"><label for="mce-group[22101]-22101-2">I am an alumni or in recovery</label>
						<input type="checkbox" value="8" name="group[22101][8]" id="mce-group[22101]-22101-3" checked><label for="mce-group[22101]-22101-3">I am an industry professional</label>
					</div>
					<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="sl_button sl_button--small">
				</div>
			</form>
			<div id="mce-responses">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    
			<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[15]='MMERGE15';ftypes[15]='zip';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';fnames[6]='ACCT_OWNER';ftypes[6]='text';fnames[7]='MMERGE7';ftypes[7]='text';fnames[9]='MMERGE9';ftypes[9]='text';fnames[10]='MMERGE10';ftypes[10]='text';fnames[11]='MMERGE11';ftypes[11]='text';fnames[12]='MMERGE12';ftypes[12]='text';fnames[14]='MCMPGN';ftypes[14]='text';fnames[8]='URL';ftypes[8]='text';fnames[13]='MMERGE13';ftypes[13]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
		</div>
		<!--End mc_embed_signup-->

		<?php

		$emailform = ob_get_clean();
		$content = '[shortcode_unautop]<div class="sl_content-locker"><div class="sl_content-locker__form"><div class="sl_card sl_card--content-locker"><img src="' . get_site_url()  .'/wp-content/themes/slate/dist/img/icon/lock-icon.svg"><h3>' . esc_attr($specs['heading']) . '</h3>' . $emailform .'</div></div><div class="sl_content-locker__content">' . do_shortcode ( $content ) . '</div></div>[/shortcode_unautop]';

		$content = str_replace(array("\r", "\n",'', '<p>', '</p>'), '', $content);

	    return $content;
		}

	add_shortcode ('content-locker', 'sl_content_locker' );
///CONTAINER
?>

