<?php
// CTA
	function sl_assessment( $atts ) {
		$specs = shortcode_atts( array(
			'type'	=> 'self', 
			), $atts );
		if(in_array('loved-one', $specs, true)){
			return '<div class="sl_callout sl_callout--assessment">
						<div class="sl_row">
							<div class="sl_cell medium-6">
								<h3>Are you concerned for a loved one?</h3>
							</div>
							<div class="sl_cell medium-6">
								<a class="sl_button sl_button--secondary sl_assessment sl_love-assessment">Loved one assessment</a>
							</div>
						</div>
					</div>';
			} else {
			return '<div class="sl_callout sl_callout--assessment">
						<div class="sl_row">
							<div class="sl_cell medium-6">
								<h3>We understand that you may not be ready to call yet</h3>
							</div>
							<div class="sl_cell medium-6">
								<a class="sl_button sl_button--secondary sl_assessment sl_self-assessment">Take our self-assessment</a>
							</div>
						</div>
					</div>';
			};

	}
	add_shortcode( 'assessment', 'sl_assessment' );
///CTA
?>