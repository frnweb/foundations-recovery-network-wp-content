<?php
// Webinar Shortcode Query
function sl_webinar_query( $atts, $content = null ) {
    global $post;

    $html = "";

    $specs = shortcode_atts( array(
        'id'   => null,
    ), $atts );

    $webinar_query = new WP_Query( array(
        'post_type' => 'sl_webinars_cpts',
        'p' => $specs['id'],
    ) );

    $current_id = $specs['id'];

    if( $webinar_query->have_posts() ) : while( $webinar_query->have_posts() ) : $webinar_query->the_post();

        $webinar_title = get_field('webinar_title', $current_id);    
        $guest_speaker = get_field('guest_speaker', $current_id);
        $webinar_date = get_field('webinar_date', $current_id);
        $webinar_summary = get_field('webinar_summary', $current_id);
        $webinar_register_url = get_field('webinar_register_url', $current_id);
        $webinar_video = get_field('video', $current_id);
        $webinar_image = get_field('webinar_image', $current_id);
        $webinar_ceu = get_field('add_ceu', $current_id);

        $date = date('d/m/Y h:i a');
        

        $myDateTime = DateTime::createFromFormat('d/m/Y h:i a', $webinar_date);
        $formattedwebinardate = $myDateTime->format('l, F jS, Y');
        $formattedwebinartime = $myDateTime->format('g:iA T');

        $html .= '<div class="sl_webinar--callout">';
            $html .= '<div class="sl_row">';

                $html .= '<div class="sl_cell large-2 medium-2">';
                if ($webinar_image['url'] == '' ) {
                    $html .= '<img class="sl_webinar--callout__image" src= "/wp-content/themes/slate/dist/img/webinar/Webinar_Fallback.jpg">';
                } else {
                    $html .= '<img class="sl_webinar--callout__image" src= "' . $webinar_image['url'] . '">';
                }
                $html .= '</div>';//end sl_cell large-2

                $html .= '<div class="sl_cell xlarge-6 large-5 medium-5">';
                    $html .= '<h3 class="sl_webinar--callout__title">' . $webinar_title . "</h3>";
                    $html .= '<p class="sl_webinar--callout__speaker">Presented by ' . $guest_speaker . "</p>";
                $html .= '</div>';//end sl_cell large-6

                $html .= '<div class="sl_cell xlarge-4 large-5 medium-5">';
                    // $html .= '<div class="sl_webinar--callout__date">';
                    //     $html .= '<p>' . $formattedwebinardate . "</p>";
                    //     $html .= '<p>' . $formattedwebinartime . "</p>";
                    // $html .= '</div>';// end .sl_webinar--callout__date
                    if ( $webinar_video != '') {
                        $html .=  '<a class="sl_button sl_button--secondary" data-open="sl_video--webinar">Play Video</a>';
                        $html .= '<div class="reveal sl_reveal sl_reveal--video" id="sl_video--webinar" data-reveal data-reset-on-close="true"><div class="responsive-embed widescreen">' . $webinar_video . '</div><button class="close-button" data-close aria-label="Close modal" type="button"><span class="fa fa-times"></span></button></div>';
                    } elseif ($webinar_date > $date && $webinar_register_url != '' ) {
                        $html .=  '<a href="' . $webinar_register_url . '" class="sl_button">Register</a>';
                    }else {
                    }
                $html .= '</div>';//end sl_cell large-4

                $html .= '</div>';//end sl_row
        $html .= '</div>';// end sl_webinar--callout


    endwhile; endif;

    return $html;

}
add_shortcode ('sl_webinar', 'sl_webinar_query' );
///Webinar
?>