<?php 
//This page lists all whitepaper child pages

get_header(); ?>
<?php
	if(get_post_meta($post->ID, 'pyre_full_width', true) == 'yes') {
		$content_css = 'width:100%';
		$sidebar_css = 'display:none';
	}
	elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'left') {
		$content_css = 'float:right;';
		$sidebar_css = 'float:left;';
	} elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'right') {
		$content_css = 'float:left;';
		$sidebar_css = 'float:right;';
	}
	?>
	
	<?php the_content(); ?>
	
	<?php $args = array(
	'post_type' => 'page',
	'paged' => get_query_var( 'paged' ),
	'post_parent' => 166
	); 
	//refer to http://codex.wordpress.org/Class_Reference/WP_Query for variable options
	
	?>
	<?php // The Query
	$the_query = new WP_Query( $args );
	?> 
	
	
	<div id="content" style="<?php echo $content_css; ?>">
		<!--found: <?=$the_query->found_posts; ?>-->
		<!--display: <?=$the_query->post_count;?>-->
		<!--pages: <?=$the_query->max_num_pages;?>-->
		<?php if ( $the_query->have_posts() ) : ?>
		<?php while($the_query->have_posts()): $the_query->the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php
			/*
			if($data['featured_images']):
			if($data['legacy_posts_slideshow']) {
				include('legacy-slideshow.php');
			} else {
				include('new-slideshow.php');
			}
			endif;
			*/
			?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="post-content">
				<?php
				if($data['content_length'] == 'Excerpt') {
					if($data['strip_html_excerpt']) {
						$stripped_content = strip_shortcodes( strip_tags( tf_content( $data['excerpt_length_blog']  ) ) );
						echo $stripped_content; 
					} else {
						//$stripped_content = strip_shortcodes( tf_content( $data['excerpt_length_blog']  ) );
						//My original:
						$content = strip_shortcodes( tf_content($data['excerpt_length_blog'])); //apply_filters( 'the_content', "") applies any plugin filters get_the_content('Read more')
						$content = str_replace( ']]>', ']]&gt;', $content );
						
						//checks if a header is in the content
						//striping the shortcodes in the content at this point might cause an issue when stripping out shortcodes from the very front of the content
						$header_found=strpos($content,"<h2");
						if($header_found) $header_type="h2";
						/* //hiding this since PDF tables use h3 and all whitepapers basically have h2s only. If other headers should be scanned, then we need to rotate through them in DOM
						if(!$header_found) {
							$header_found=strpos($content,"<h1");
							if($header_found) $header_type="h1";  //since it was found
						}
						if(!$header_found) {
							$header_found=strpos($content,"<h3");
							if($header_found) $header_type="h3"; //since it was found
						}
						*/
						
						if($header_found) {
							//If header found, convert to object modeling method and remove headers and redisplay on page (assumes object modeling is more intensive processing)
							libxml_use_internal_errors(true);
							$dom = new DOMDocument;
							$dom->loadHTML( mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8') );
							$xpath = new DomXPath($dom); 
							libxml_clear_errors();
							$book = $dom->documentElement;
							$headers = $book->getElementsByTagName($header_type); //gets all headers on the page to rotate through and remove them
							//echo "<!--length:".$headers->length."; type:".$header_type."-->";
							if($headers->length > 0) {
								$domElemsToRemove = array();
								foreach ( $headers as $domElement ) { 
									$domElemsToRemove[] = $domElement; 
								}
								foreach ($domElemsToRemove as $header) {
									//echo "<!--".$header."-->";
									$header->parentNode->removeChild($header);
								}
								//replace pattern removes the doctype stuff WP adds to content for some reason
								echo preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $dom->saveHTML());
							}
							else echo $content; //if the dom fails
						} 
						else echo $content; //when no headers found
					}
					//echo $stripped_content; //old way. moved it up before else. activate this if removing newer code for stripping headers
				} else {
					the_content(); //if length is not to act like an excerpt
				}
				//if($data['post_meta']): ?>
			<div class="meta-info"><?php 
				if($hide=="no") { ?>
				<!--<div class="alignleft">
						<?php //the_time($data['date_format']); ?><span class="sep">|</span><?php the_category(', '); ?><span class="sep">|</span><?php //comments_popup_link(__('0 Comments', 'Avada'), __('1 Comment', 'Avada'), '% '.__('Comments', 'Avada')); 
						 ?>
				</div>--><?php 
				} ?>
				
				<div class="alignright">
					<a href="<?=get_permalink(); ?>" class="read-more"><?php echo __('Read More', 'Avada'); ?></a>
				</div>
			</div>
			<?php //endif; ?>
			</div>
			<div style="clear:both;"></div>
			
		</div>
		<?php endwhile; ?>
		
		<?php themefusion_pagination($pages = $the_query->max_num_pages, $range = 2); ?>
		
		
	</div>
	<?php else:  ?>
		<p><?php _e( 'Sorry, no white papers matched our criteria.' ); ?></p>
	<?php endif; 
	wp_reset_postdata();
	?>
	<?php wp_reset_query(); ?>
	
	<div id="sidebar" style="<?php echo $sidebar_css; ?>"><?php generated_dynamic_sidebar(); ?></div>
<?php get_footer(); ?>