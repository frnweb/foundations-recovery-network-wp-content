<?php
add_action('widgets_init', 'contact_info_load_widgets');

function contact_info_load_widgets()
{
	register_widget('Contact_Info_Widget');
}

class Contact_Info_Widget extends WP_Widget {
	
	function Contact_Info_Widget()
	{
		$widget_ops = array('classname' => 'contact_info', 'description' => '');

		$control_ops = array('id_base' => 'contact_info-widget');

		$this->WP_Widget('contact_info-widget', 'Avada: Contact Info', $widget_ops, $control_ops);
	}
	
	function widget($args, $instance)
	{
		extract($args);
		
		echo $before_widget;
		
		//Prep Title and Schema Type
		$title = apply_filters('widget_title', $instance['title']);
		
		if( $instance['address'] || $instance['phone'] || $instance['fax'] || $instance['email'] || $instance['web'] ) {
			if($title!="") {
				$title_place = $title;
				if($title!="Foundations Recovery Network") {
					$title="Contact ".$title; 
				}
			}
		}

		if(($title!="Foundations Recovery Network" || $title=="") && $instance['address'] || $instance['city'] || $instance['state'] || $instance['zip']) : ?>
		<div itemscope itemtype="http://schema.org/MedicalOrganization">
		<?php 
		elseif($instance['address'] || $instance['city'] || $instance['state'] || $instance['zip']) : 
		?>
		<div itemscope itemtype="http://schema.org/Organization">
		<?php endif;
		
		//Print title
		if($title=="Foundations Recovery Network") echo $title;
		elseif($title) echo $before_title.$title.$after_title; ?>
		
		<?php if($instance['loc_name']): ?>
		<span itemprop="name"><?php echo $instance['loc_name']; ?></span><br />
		<?php endif;
		
		//Contact Info
		if($instance['address'] || $instance['city'] || $instance['state'] || $instance['zip']): ?>
			<a itemprop="hasMap" href="<?php 
			if($instance['gmaps']): ?>
			<?php echo $instance['gmaps']; ?><?php 
			else: ?><?php echo "http://maps.google.com/?q="; ?><?=($instance['address']) ? str_replace("<br />",", ",str_replace("<br>",", ",$instance['address'])) : "" ; ?><?=($instance['address']!="" && $instance['state']!="") ? ", " : ""; ?><?=($instance['city']) ? $instance['city'].", " : ""; ?><?=($instance['state']) ? $instance['state']." " : ""; ?><?=($instance['zip']) ? $instance['zip'] : ""; ?><?php 
			endif; 
			?>" target="_blank">
			<p class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<?php endif; ?>
		
		<?php if($instance['address']): ?>
		<span itemprop="streetAddress"><?php echo $instance['address']; ?></span><br />
		<?php endif; ?>
		
		<?php if($instance['city']): ?>
		<span itemprop="addressLocality"><?php echo $instance['city']; ?></span>, 
		<?php endif; ?>
		
		<?php if($instance['state']): ?>
		<span itemprop="addressRegion"><?php echo $instance['state']; ?></span> 
		<?php endif; ?>
		
		<?php if($instance['zip']): ?>
		<span itemprop="postalCode"><?php echo $instance['zip']; ?></span>
		<?php endif; ?>
		
		<?php if($instance['address'] || $instance['city'] || $instance['state'] || $instance['zip']): ?>
		</p></a>
		<?php endif; ?>

		<?php if($instance['phone']): ?>
		<div class="phone"><?php _e('Phone:', 'Avada'); ?> <span itemprop="telephone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sidebar (Contact Widget)" ga_phone_label="'.$instance['phone'].'" css_style="text-decoration:underline;" number="'.$instance['phone'].'"]'); ?></span></div>
		<?php endif; ?>

		<?php if($instance['fax']): ?>
		<div class="fax"><?php _e('Fax:', 'Avada'); ?> <span itemprop="faxNumber"><?php echo $instance['fax']; ?></span></div>
		<?php endif; ?>

		<?php if($instance['email']): ?>
		<div class="email"><?php _e('Email:', 'Avada'); ?> <span itemprop="email"><a href="mailto:<?php echo $instance['email']; ?>"><?php if($instance['emailtxt']) { echo $instance['emailtxt']; } else { echo $instance['email']; } ?></a></span></div>
		<?php endif; ?>

		<div>&nbsp;</div>
		
		<?php if($instance['web']): ?>
		<?php /* //if($instance['gplaces'] || $instance['facebook'] || $instance['twitter'] || $instance['linkedin']): ?>
		<span class="web"><a itemprop="url" href="<?php echo "http://".str_replace("http://","",$instance['web']); ?>" rel="nofollow"><?php 
			if(!$instance['icons']) : ?>
				<img src="<?php bloginfo( 'template_url' );?>/images/btn_website.png" style="height:25px;" alt="<?=($instance['webtxt']) ? $instance['webtxt'] : $instance['web']; ?>" title="Visit Our Website" />
			<?php else : ?><?php _e('Website', 'Avada'); ?><br />
			<?php endif; 
		?></a><?=($instance['icons']=="" && ($instance['gplaces'] || $instance['facebook'] || $instance['twitter'] || $instance['linkedin'])) ? " | " : "";?></span>
		<?php else : */?>
		<div class="web"><?php _e('Website: ', 'Avada'); ?><a itemprop="url" href="<?php echo "http://".str_replace("http://","",$instance['web']); ?>" rel="nofollow"><?php if($instance['webtxt']) { echo $instance['webtxt']; } else { echo str_replace("http://","",$instance['web']); } ?></a></div>
		<?php //endif; ?>
		<?php endif; ?>
		
		<?=($instance['icons']=="" && ($instance['gplaces'] || $instance['facebook'] || $instance['twitter'] || $instance['linkedin'])) ? " | " : "";?>
		<?=($instance['icons']!="") ? "
		<br />Follow Us: <br />
		" : ""; ?>
		
		<?php if($instance['gplaces']): ?>
		<span class="web" itemprop="sameAs"><a href="<?php echo $instance['gplaces']; ?>" target="_blank"><?php 
			if($instance['icons']) : ?>
				<img src="<?php bloginfo( 'template_url' );?>/images/btn_googleplus.png" style="height:25px;" alt="<?=$title_place;?> Google+ Profile" title="Follow us on Google+" />
			<?php else : ?><?php _e('Google+', 'Avada'); ?>
			<?php endif; 
		?></a></span><?=($instance['icons']=="" && ($instance['facebook'] || $instance['twitter'] || $instance['linkedin'])) ? " | " : "";?>
		<?php endif; ?>
		
		<?php if($instance['facebook']): ?>
		<span class="web" itemprop="sameAs"><a href="<?php echo $instance['facebook']; ?>" target="_blank"><?php 
			if($instance['icons']) : ?>
				<img src="<?php bloginfo( 'template_url' );?>/images/btn_facebook_sm.png" style="height:25px;" alt="<?=$title_place;?> Facebook Profile" title="Follow us on Facebook" />
			<?php else : ?><?php _e('Facebook', 'Avada'); ?>
			<?php endif; 
		?></a></span><?=($instance['icons']=="" && ($instance['twitter'] || $instance['linkedin'])) ? " | " : "";?>
		<?php endif; ?>
		
		<?php if($instance['twitter']): ?>
		<span class="web" itemprop="sameAs"><a href="<?php echo $instance['twitter']; ?>" target="_blank"><?php 
			if($instance['icons']) : ?>
				<img src="<?php bloginfo( 'template_url' );?>/images/btn_twitter.png" style="height:25px;" alt="<?=$title_place;?> Twitter Profile" title="Follow us on Twitter" /> 
			<?php else : ?><?php _e('Twitter', 'Avada'); ?>
			<?php endif; 
		?></a></span><?=($instance['icons']=="" && ($instance['linkedin'])) ? " | " : "";?>
		<?php endif; ?>
		
		<?php if($instance['linkedin']): ?>
		<span class="web" itemprop="sameAs"><a href="<?php echo $instance['linkedin']; ?>" target="_blank"><?php 
			if($instance['icons']) : ?>
				<img src="<?php bloginfo( 'template_url' );?>/images/btn_linkedin.png" style="height:25px;" alt="<?=$title_place;?> LinkedIn Profile" title="Follow us on LinkedIn" />
			<?php else : ?><?php _e('LinkedIn', 'Avada'); ?>
			<?php endif; 
		?></a></span>
		<?php endif; ?>
		
		<?=($instance['icons']=="" && ($instance['gplaces'] || $instance['facebook'] || $instance['twitter'] || $instance['linkedin'])) ? " | " : "";?>
		
		<?php if($instance['gmaps']): ?>
		<div>&nbsp;</div>
		<?php endif; ?>
				
		<?php if($instance['gmap_iframe']): ?>
		<?php echo $instance['gmap_iframe']; ?>
		<?php endif; ?>
		
		<?php if($instance['address'] || $instance['city'] || $instance['state'] || $instance['zip']): ?>
		</div>
		<?php endif; ?>
		
		<?=$after_widget;?>

		<?php
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = $new_instance['title'];
		$instance['loc_name'] = $new_instance['loc_name'];
		$instance['address'] = $new_instance['address'];
		$instance['city'] = $new_instance['city'];
		$instance['state'] = $new_instance['state'];
		$instance['zip'] = $new_instance['zip'];
		$instance['phone'] = $new_instance['phone'];
		$instance['fax'] = $new_instance['fax'];
		$instance['email'] = $new_instance['email'];
		$instance['emailtxt'] = $new_instance['emailtxt'];
		$instance['icons'] = $new_instance['icons'];
		$instance['web'] = $new_instance['web'];
		$instance['webtxt'] = $new_instance['webtxt'];
		$instance['gmaps'] = $new_instance['gmaps'];
		$instance['gmap_iframe'] = $new_instance['gmap_iframe'];
		$instance['gplaces'] = $new_instance['gplaces'];
		$instance['facebook'] = $new_instance['facebook'];
		$instance['twitter'] = $new_instance['twitter'];
		$instance['linkedin'] = $new_instance['linkedin'];

		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Contact Info');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Business Name:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_html($instance['title']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('loc_name'); ?>">Location Name (if applies):</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('loc_name'); ?>" name="<?php echo $this->get_field_name('loc_name'); ?>" value="<?php echo esc_html($instance['loc_name']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('address'); ?>">Address:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" value="<?php echo esc_html($instance['address']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('address'); ?>">City:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('city'); ?>" name="<?php echo $this->get_field_name('city'); ?>" value="<?php echo esc_html($instance['city']); ?>" /><br />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('address'); ?>">State/Zip:</label>
			<input class="widefat" style="width: 50px;" id="<?php echo $this->get_field_id('state'); ?>" name="<?php echo $this->get_field_name('state'); ?>" value="<?php echo esc_html($instance['state']); ?>" />
			<input class="widefat" style="width: 160px;" id="<?php echo $this->get_field_id('zip'); ?>" name="<?php echo $this->get_field_name('zip'); ?>" value="<?php echo esc_html($instance['zip']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('gmaps'); ?>">Google Maps Link:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('gmaps'); ?>" name="<?php echo $this->get_field_name('gmaps'); ?>" value="<?php echo esc_html($instance['gmaps']); ?>" /><br />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('gmap_iframe'); ?>">Google Maps Embed Code:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('gmap_iframe'); ?>" name="<?php echo $this->get_field_name('gmap_iframe'); ?>" value="<?php echo esc_html($instance['gmap_iframe']); ?>" /><br />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('phone'); ?>">Phone:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" value="<?php echo esc_html($instance['phone']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('fax'); ?>">Fax:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('fax'); ?>" name="<?php echo $this->get_field_name('fax'); ?>" value="<?php echo esc_html($instance['fax']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('email'); ?>">Email:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" value="<?php echo esc_html($instance['email']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('emailtxt'); ?>">Email Link Text:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('emailtxt'); ?>" name="<?php echo $this->get_field_name('emailtxt'); ?>" value="<?php echo esc_html($instance['emailtxt']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('icons'); ?>">Use Icons?:</label>
			<input type="checkbox" id="<?php echo $this->get_field_id('icons'); ?>" name="<?php echo $this->get_field_name('icons'); ?>" value="1" <?=($instance['icons']) ? "checked=\"checked\"" : "";?> />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('web'); ?>">Website URL (with HTTP):</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('web'); ?>" name="<?php echo $this->get_field_name('web'); ?>" value="<?php echo esc_html($instance['web']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('webtxt'); ?>">Website URL Text:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('webtxt'); ?>" name="<?php echo $this->get_field_name('webtxt'); ?>" value="<?php echo esc_html($instance['webtxt']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('gplaces'); ?>">Google Places:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('gplaces'); ?>" name="<?php echo $this->get_field_name('gplaces'); ?>" value="<?php echo esc_html($instance['gplaces']); ?>" /><br />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('facebook'); ?>">Facebook:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" value="<?php echo esc_html($instance['facebook']); ?>" /><br />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('twitter'); ?>">Twitter:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" value="<?php echo esc_html($instance['twitter']); ?>" /><br />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('linkedin'); ?>">LinkedIn:</label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('linkedin'); ?>" name="<?php echo $this->get_field_name('linkedin'); ?>" value="<?php echo esc_html($instance['linkedin']); ?>" /><br />
		</p>
	<?php
	}
}
?>