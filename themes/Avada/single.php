<?php get_header(); ?>
	<?php

	//////  NIGEL BLOG STYLING CONTROLS  ///////
	// Triggers Nigel's Blog styling
	$nigel_catID = 23;
	$categories = get_the_category(); 
	foreach ($categories as $category) {
		if($category->cat_ID==$nigel_catID) {
			$nigel_title_css = ' class="nigel_title" ';
		}
	}
	//End Nigel styling controls

	if(get_post_meta($post->ID, 'pyre_full_width', true) == 'yes') {
		$content_css = 'width:100%';
		$sidebar_css = 'display:none';
	}
	elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'left') {
		$content_css = 'float:right;';
		$sidebar_css = 'float:left;';
	} 
	elseif(get_post_meta($post->ID, 'pyre_sidebar_position', true) == 'right') {
		$content_css = 'float:left;';
		$sidebar_css = 'float:right;';
	}
	?>
	<div id="content" style="<?php echo $content_css; ?>">
		<?php 
			// 2/2/18 
			//	- Dax not sure why this code is here, but it changed the main query for the page causing problems elsewhere.
			//	- The $query_string var not defined on this page, so it effectively wasn't working already.
			//wp_reset_query();
			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//query_posts($query_string.'&paged='.$paged); 
		

		if($hide=="no") { //removed these links but kept the code just in case ?>
		<div class="single-navigation clearfix">
			<?php previous_post_link('%link', __('Previous', 'Avada')); ?>
			<?php next_post_link('%link', __('Next', 'Avada')); ?>
		</div>
		<?php 
		} 




		if(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php
			global $data;  //specific to theme

			/*
			if((!$data['legacy_posts_slideshow'] && !$data['posts_slideshow']) && get_post_meta($post->ID, 'pyre_video', true)): ?>
			<!--<div class="flexslider post-slideshow">
				<ul class="slides">
					<li class="full-video">
						<?php echo get_post_meta($post->ID, 'pyre_video', true); ?>
					</li>
				</ul>
			</div>-->
			<?php endif;
			*/

			//Get all attachments to post
			//issue: if we remove an image, it'll still be returned here. There is no way to remove it from the DB.
			if($data['featured_images_single']):
				if($data['legacy_posts_slideshow']):
					$args = array(
					    'post_type' => 'attachment',
					    'numberposts' => $data['posts_slideshow_number']-1,
					    'post_status' => null,
					    'post_parent' => $post->ID,
						'orderby' => 'menu_order',
						'order' => 'ASC',
						'post_mime_type' => 'image',
						'exclude' => get_post_thumbnail_id()
					);
					$attachments = get_posts($args);

					//If the post has a thumbnail or video stored with post
					if((has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true))):
			?>
			<div class="flexslider post-slideshow">
				<ul class="slides">
					<?php 
						// Check for video first (stored as a custom field)
						if(get_post_meta($post->ID, 'pyre_video', true)): ?>
					<li class="full-video">
						<?php echo get_post_meta($post->ID, 'pyre_video', true); ?>
					</li>
					<?php 
						endif; //pyre video

						//Now get standard post image if a video wasn't stored
						if(has_post_thumbnail() && !get_post_meta($post->ID, 'pyre_video', true)):
							$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
							$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
							$attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id()); 
					?>
					<li>
						<a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto[gallery<?php the_ID(); ?>]"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment_data['image_meta']['title']; ?>" /></a>
					</li>
					<?php 
						endif; //thumbnail

						//Now get all attachments to post -- any image ever inserted into it
						if($data['posts_slideshow']):
							foreach($attachments as $attachment):
								$attachment_image = wp_get_attachment_image_src($attachment->ID, 'full');
								$full_image = wp_get_attachment_image_src($attachment->ID, 'full');
								$attachment_data = wp_get_attachment_metadata($attachment->ID); 
					?>
					<li>
						<a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto[gallery<?php the_ID(); ?>]"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>
					</li>
					<?php 
							endforeach;
						endif; //slideshow
					?>
				</ul>
			</div>
			<?php 
					endif; // end Check for video first (stored as a custom field)
				else: //  legacy_posts_slideshow
			
					//If post has a featured image OR a video stored in custom fields
					if((has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true))):
			?>
			<div class="flexslider post-slideshow">
				<ul class="slides">
					<?php 

						//check if video stored in custom fields
						if(get_post_meta($post->ID, 'pyre_video', true)): 
					?>
					<li class="full-video">
						<?php echo get_post_meta($post->ID, 'pyre_video', true); ?>
					</li>
					<?php 
						endif; 
					
						//Now get featured image only if video NOT stored in custom fields
						if(has_post_thumbnail() && !get_post_meta($post->ID, 'pyre_video', true)):
							$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); 
							$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); 
							$attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id()); 
					?>
					<li>
						<a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto[gallery<?php the_ID(); ?>]"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment_data['image_meta']['title']; ?>" /></a>
					</li>
					<?php 
						endif; //end check for featured image as long as no video stored
					

						//If slideshow is activated, check for theme version of featured image
						if($data['posts_slideshow']): 
							$i = 2;
							while($i <= $data['posts_slideshow_number']):
								$attachment_new_id = kd_mfi_get_featured_image_id('featured-image-'.$i, 'post');
								if($attachment_new_id):
									$attachment_image = wp_get_attachment_image_src($attachment_new_id, 'full'); 
									$full_image = wp_get_attachment_image_src($attachment_new_id, 'full'); 
									$attachment_data = wp_get_attachment_metadata($attachment_new_id); 
					?>
					<li>
						<a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto[gallery<?php the_ID(); ?>]">
							<img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment_data['image_meta']['title']; ?>" />
						</a>
					</li>
					<?php 
								endif; 
								$i++; 
							endwhile; 
						endif; //posts_slideshow
					?>
				</ul>
			</div>
			<?php 
					endif; //If post has a featured image OR a video stored in custom fields 
				endif; //legacy_posts_slideshow
			endif; //featured_images_single
			


			//if blog post title turned on...
			if($data['blog_post_title']): 
			?>
			<h2 <?=$nigel_title_css; ?>><?php the_title(); ?></h2>
			<?php 
			endif; 
			?>
			<div class="post-content">
				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
			</div>
			<?php 


			if($data['social_sharing_box']): ?>
			<div style="clear:both;"></div>
			<div class="share-box">
				<h4><?php echo __('Share This Story, Choose Your Platform!', 'Avada'); ?></h4>
				<ul class="social-networks">
					<?php if($data['sharing_facebook']): ?>
					<li class="facebook">
						<a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>">
							Facebook
						</a>
						<div class="popup">
							<div class="holder">
								<p>Facebook</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php if($data['sharing_twitter']): ?>
					<li class="twitter">
						<a href="http://twitter.com/home?status=<?php the_title(); ?> <?php the_permalink(); ?>">
							Twitter
						</a>
						<div class="popup">
							<div class="holder">
								<p>Twitter</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php if($data['sharing_linkedin']): ?>
					<li class="linkedin">
						<a href="http://linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>">
							LinkedIn
						</a>
						<div class="popup">
							<div class="holder">
								<p>LinkedIn</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php if($data['sharing_reddit']): ?>
					<li class="reddit">
						<a href="http://reddit.com/submit?url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>">
							Reddit
						</a>
						<div class="popup">
							<div class="holder">
								<p>Reddit</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php if($data['sharing_tumblr']): ?>
					<li class="tumblr">
						<a href="http://www.tumblr.com/share/link?url=<?php echo urlencode(get_permalink()); ?>&amp;name=<?php echo urlencode($post->post_title); ?>&amp;description=<?php echo urlencode(get_the_excerpt()); ?>">
							Tumblr
						</a>
						<div class="popup">
							<div class="holder">
								<p>Tumblr</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php if($data['sharing_google']): ?>
					<li class="google">
						<a href="http://google.com/bookmarks/mark?op=edit&amp;bkmk=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>">
							Google +1
						</a>
						<div class="popup">
							<div class="holder">
								<p>Google +1</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php if($data['sharing_email']): ?>
					<li class="email">
						<a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>">
							Email
						</a>
						<div class="popup">
							<div class="holder">
								<p>Email</p>
							</div>
						</div>
					</li>
					<?php endif; ?>
				</ul>
			</div>
			<?php 
			endif; //social_sharing_box 
			

			if($data['author_info'] && (get_the_author_meta('ID')=='11' || get_the_author_meta('ID')=='14')): ?>
			<div class="about-author">
				<div class="title"><h2><?php echo __('About the Author:', 'Avada'); ?> <?php the_author_posts_link(); ?></h2></div>
				<div class="about-author-container">
					<div class="avatar">
						<?php echo get_avatar(get_the_author_meta('email'), '72'); ?>
					</div>
					<div class="description">
						<?php the_author_meta("description"); ?>
					</div>
				</div>
			</div>
			<?php 
			endif; //author


			if($data['post_meta']): ?>
			<div style="clear:both;"></div>
			<div class="meta-info">
				<div class="alignleft">
					<?php 
						if(strtolower(get_the_author_meta('public'))=='yes' ) {
							echo __('By', 'Avada'); ?> <?php the_author_posts_link(); 
							echo '<span class="sep"> | </span>';
						}
					?><?php //the_time($data['date_format']); ?><span class="sep"></span>Read more in: <?php the_category(', '); ?>
				</div>
			</div>
			<?php 
			endif; 


			if($data['related_posts'] && !$nigel_title_css): 
				$related = get_related_posts($post->ID); 
				if($related->have_posts()): 

			?>
			<div class="related-posts">
				<div class="title"><h2><?php echo __('Related Posts', 'Avada'); ?></h2></div>
				<div id="carousel" class="es-carousel-wrapper">
					<div class="es-carousel">
						<ul>
							<?php 
							while($related->have_posts()): $related->the_post(); 
								if(has_post_thumbnail()): ?>
							<li>
								<div class="image">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('related-img'); ?></a>
									<div class="image-extras">
										<div class="image-extras-content">
											<?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
											<a class="icon link-icon" href="<?php the_permalink(); ?>">Permalink</a>
											<?php
												if(get_post_meta($post->ID, 'pyre_video_url', true)) {
													$full_image[0] = get_post_meta($post->ID, 'pyre_video_url', true);
												} 
											?>
											<a class="icon gallery-icon" href="<?php echo $full_image[0]; ?>" rel="prettyPhoto[gallery]">Gallery</a>
											<h3><?php the_title(); ?></h3>
										</div>
									</div>
								</div>
							</li>
							<?php 
								endif; 
							endwhile; 
							?>
						</ul>
					</div>
				</div>
			</div>
			<?php 
				endif; //if have related posts
			endif; // if related_posts turned on and it's not a nigel post

		
			if($data['blog_comments']): 
				if($nigel_title_css) { 
					//wp_reset_query(); //2/2/18 - deactivated by Dax. I don't think this is necessary with a category query.
					//we only want comments showing on Nigel's posts
					comments_template(); 
				} 
			endif; //blog comments activated?

		?>
		</div>
		<?php 

		endif; //end check if posts returned in query 

		?>
	</div>


	<div id="sidebar" style="<?php echo $sidebar_css; ?>">
		<?php generated_dynamic_sidebar(); ?>
	</div>

<?php 

get_footer(); 

?>