<?php

/**
 * Slate required files
 * Paths are relative to the app/ directory
 */

$slate_includes = array(
		'/setup.php',				// Setup functions
		'/app.php',					// Timber class
		'/../vendor/autoload.php',	// Grab the autoload from composer								
		'/actions.php',
		'/helpers.php',
);

foreach ( $slate_includes as $file ) {
	$filepath = locate_template( '/app' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /app%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

//Redirect FAQ Post Type to FAQ Page
add_action( 'template_redirect', 'faq_redirect_post' );

function faq_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'sl_faqs_cpts' ==  $queried_post_type ) {
    wp_redirect( '/about-us/faq/', 301 );
    exit;
  }
}

// Breadcrumbs
function get_breadcrumb() {
       
    // Settings
    $separator          = '&gt;';
    $breadcrums_id      = 'sl_breadcrumbs';
    $breadcrums_class   = 'sl_breadcrumbs';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="sl_breadcrumbs__home"><a class="sl_breadcrumbs__link show-for-medium" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a><a class="hide-for-medium" href="' . get_home_url() .'"><i class="fa fa-home"></i></a></li>';
        
        if ( is_home() && !is_front_page()) {
        	echo '<li class="sl_breadcrumbs__current"><h1>Articles</h1></li>';

        } else if ( is_archive() && is_post_type_archive( 'sl_research_cpts') ) {
              
            echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/" title="For Professionals">For Professionals</a></li><li class="sl_breadcrumbs__current"><h1>White Papers</h1></li>';
              
        } else if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="sl_breadcrumbs__current"><h1>' . post_type_archive_title( '', false ) . '</h1></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {

                if ( is_tax('research-type', 'white-papers')) {
                    echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/" title="For Professionals">For Professionals</a></li>';
                } else if ( is_tax('research-type', 'research-abstracts')) {
                    echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/" title="For Professionals">For Professionals</a></li>';
                } else {
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);
                  
                    echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                }
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="sl_breadcrumbs__current"><h1>' . $custom_tax_name . '</h1></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();

            //If custom post type is locations
            if($post_type == 'sl_locations_cpts') {
              
                echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/locations/" title="Locations">Locations</a></li>';
            }

            //If custom post type is events
            elseif($post_type == 'sl_event_cpts') {
              
                echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/conferences/" title="Conferences">Conferences</a></li>';
            }
            
            //If custom post type is staff
            elseif($post_type == 'sl_staff_cpts') {
              
                echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/about-us/" title="About Us">About Us</a></li><li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/about-us/staff/" title="Staff">Staff</a></li>';
            }

            //If custom post type is webinars
            elseif($post_type == 'sl_webinars_cpts') {
              
                echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/" title="For Professionals">For Professionals</a></li><li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/educational-webinar-series/" title="Educational Webinar Series">Educational Webinar Series</a></li>';
            }

            //If custom post type is staff
            elseif($post_type == 'sl_research_cpts') {
              
                echo '<li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/" title="For Professionals">For Professionals</a></li><li class="sl_breadcrumbs__parent"><a class="sl_breadcrumbs__link" href="/for-professionals/mental-health-white-papers/" title="White Papers">White Papers</a></li>';
            }

            // If it is a custom post type display name and link
            else if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="sl_breadcrumbs__current"><h1>' . get_the_title() . '</h1></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="sl_breadcrumbs__current"><h1>' . get_the_title() . '</h1></li>';
            
            // Don't show current title if single webinar
            } else if($post_type != 'sl_webinars_cpts' && $post_type != 'sl_research_cpts') {
                  
                echo '<li class="sl_breadcrumbs__current"><h1>' . get_the_title() . '</h1></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="sl_breadcrumbs__current"><h1>' . single_cat_title('', false) . '</h1></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="sl_breadcrumbs__parent sl_breadcrumbs__parent--' . $ancestor . '"><a class="sl_breadcrumbs__link" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="sl_breadcrumbs__current"><h1>' . get_the_title() . '</h1></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="sl_breadcrumbs__current"><h1>' . get_the_title() . '</h1></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="sl_breadcrumbs__current"><h1>' . $get_term_name . '</h1></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="sl_breadcrumbs__current"><h1>'.__('Page') . ' ' . get_query_var('paged') . '</h1></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="sl_breadcrumbs__current"><h1>Search Results</h1></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li class="sl_breadcrumbs__current"><h1>' . 'Page Not Found' . '</h1></li>';
        }
       
        echo '</ul>';
           
    }//end if is not front page
       
}//end function get_breadcrumbs

//Relevanssi Posts Per Page
add_filter('post_limits', 'postsperpage');
function postsperpage($limits) {
    if (is_search()) {
        global $wp_query;
        $wp_query->query_vars['posts_per_page'] = 10;
    }
    return $limits;
}

?>