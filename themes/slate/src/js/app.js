import 'babel-polyfill'
import $ from 'jquery'
import 'slick-carousel/slick/slick'

import whatInput from 'what-input'
import chatAgent from './lib/live-agent'
import googleMaps from './lib/google-maps'
import gaEvents from './lib/ga-events'
import slickCarousel from './lib/slick-carousel'
import hamburger from './lib/hamburger'
import facilityModule from './lib/facility-module'
import typeForms from './lib/typeforms'
import infiniteScroll from './lib/infinite-scroll'
import contactMobile from './lib/contact-mobile'
import backTop from './lib/back-to-top'
import responsiveEmbed from './lib/responsive-embed'
import contentLocker from './lib/content-locker'
// import emailMarketing from './lib/email-marketing'


window.jQuery = $;
window.$ = $;

import Foundation from 'foundation-sites'

$(document).foundation();

// Typeform
//Check if the .sl_assessment is on the page, before loading the js
if( document.querySelector('.sl_assessment') !== null ) {
	typeForms()
}

// Chat Agent
	//chatAgent()

$(document).ready(function() {
	if ($('#sl_notification-bar').hasClass("sl_banner_shown")){
		$(".sl_wrapper").addClass('sl_banner_wrapper')
		$(".sl_wrapper").append('<style> @media screen and (max-width: 47.99875em) { .sl_banner_wrapper:before { height:285px !important;} }</style>');
	} 

	$('.close-button').click(function() {
		$('#sl_notification-bar').removeClass("sl_banner_shown")
		$('.sl_wrapper').removeClass("sl_banner_wrapper")
	});
});
