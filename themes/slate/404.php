<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();

$s = str_replace(str_split('-\\/:*?"<>|'),' ',$_SERVER['REQUEST_URI']);

if (function_exists('relevanssi_do_query')) {
    $url_terms_search = new WP_Query();
    $url_terms_search->query_vars['s']				=$s;
    $url_terms_search->query_vars['posts_per_page']	=6;
    $url_terms_search->query_vars['paged']			=0;
    $url_terms_search->query_vars['post_status']	='publish';
    $context['posts'] = relevanssi_do_query($url_terms_search);
 }
 else {
    //global $wpdb;
    $url_terms_search = new WP_Query( array( 
        's' => $s, 
        //'page_id' => 26,
        'post_type' => 'any', //array( 'post', 'page' ),
        'posts_per_page' => 8,
        'post_status' => 'publish'
    ));
    $context['posts'] = Timber::get_posts($url_terms_search);
 }

$new_args = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'category__not_in' => array(61), //Exclude Press Releases
    'order'          => 'DESC',
    'orderby'        => 'date',
);

$context['get_newest'] = new Timber\PostQuery( $new_args );
 
Timber::render( '404.twig', $context );
