<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$podcasts = new FieldsBuilder('Podcasts');

$podcasts
    ->setLocation('post_type', '==', 'sl_podcasts_cpts');
  
$podcasts
	->addText('podcast_title', [
		'label' => 'Podcast Title'
	])

	->addText('episode_number', [
		'label' => 'Episode Number'
	])

	->addWysiwyg('podcast_summary', [
		'label' => 'Podcast Summary'
	])

	->addText('episode_time', [
		'label' => 'Episode Time'
	])

	->addImage('podcast_image', [
		'label' => 'Podcast Image'
	])

	->addText('guest_name', [
		'label' => 'Guest Speaker'
	])

	->addText('embed_src', [
		'label' => 'Embed Source'
	])

	->addCheckbox('player_urls', [
		'layout' => 'vertical',
		'toggle' => 1,
		'wrapper' => ['width' => 80]
	])

	->addChoices(
		['itunes' => 'Itunes'],
		['spotify' => 'Spotify'],
		['soundcloud' => 'SoundCloud'],
		['google_play' => 'Google Play'],
		['stitcher' => 'Stitcher']
	)

	->addUrl('Itunes')
	  ->conditional('player_urls', '==', 'itunes')

	->addUrl('Spotify')
	  ->conditional('player_urls', '==', 'spotify')

	->addUrl('SoundCloud')
	  ->conditional('player_urls', '==', 'soundcloud')

	->addUrl('GooglePlay')
	  ->conditional('player_urls', '==', 'google_play')

	->addUrl('Stitcher')
	  ->conditional('player_urls', '==', 'stitcher');

return $podcasts;