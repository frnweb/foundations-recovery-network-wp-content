<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$events = new FieldsBuilder('event_fields');

$events
    ->setLocation('post_type', '==', 'sl_event_cpts');
  
$events
	->addText('event_name', [
		'label' => 'Event Name'
	])

	->addDatePicker('date_start', [
		'label' => 'Event Start',
		'wrapper' => ['width' => 50],
		'display_format' => 'm/d/Y',
        'return_format' => 'm/d/Y'
	])

	->addDatePicker('date_end', [
		'label' => 'Event End',
		'wrapper' => ['width' => 50],
		'display_format' => 'm/d/Y',
        'return_format' => 'm/d/Y'
	])

	//Event Summary
	->addTextArea('event_summary', [
        'label' => 'Event Summary'
    ])

	// Video
	->addWysiwyg('video', ['label' => 'Video(iframe)','ui' => $config->ui])

	->addUrl('register_url', [
		'label' => 'Register URL'
	])

	->addUrl('sponsor_url', [
		'label' => 'Sponsor URL'
	])
	->addImage('logo_field', [
		'label' => 'Logo'
	])

	// Address Schema Group
	->addGroup('address_schema', [
		'label' => 'Event Location'
	])
	    ->addText('city', [
	        'label' => 'City',
	        'ui' => $config->ui,
	        'wrapper' => $config->wrapper
	    ])
	    	->setInstructions('Put the City i.e. Nashville')
	    ->addText('state', [
	        'label' => 'State',
	        'ui' => $config->ui,
	        'wrapper' => $config->wrapper
	    ])
	    	->setInstructions('Put the State here i.e. TN')
	->endGroup();

//Adding Modules to Post Type
$modules = new FieldsBuilder('Modules');

$modules
  ->setLocation('post_type', '==', 'sl_event_cpts');
  
$modules
  ->addFields(get_field_partial('partials.builder'));

// Footer Options Tabs & Fields
$eventsfields = new FieldsBuilder('events_fields');

	$eventsfields
		->setLocation('post_type', '==', 'sl_event_cpts')
		// Social Icons Tab
		->addTab('Event Info', ['placement' => 'top'])
			->addFields($events)			
		// Accrediations Tab
		->addTab('Page Layout', ['placement' => 'top'])
			->addFields($modules);

		return $eventsfields;