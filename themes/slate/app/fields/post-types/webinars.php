<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$webinars = new FieldsBuilder('Webinars');

$webinars
    ->setLocation('post_type', '==', 'sl_webinars_cpts')
  
	->addText('webinar_title', [
		'label' => 'Webinar Title'
	])

	->addText('guest_speaker', [
		'label' => 'Guest Speaker'
	])

	->addDateTimePicker('webinar_date', [
		'label' => 'Webinar Date' 
	])

	//Webinar Summary
	->addTextArea('webinar_summary', [
        'label' => 'Webinar Summary'
    ])

	->addUrl('webinar_register_url', [
		'label' => 'Register URL'
	])

	// Video
	->addWysiwyg('video', ['label' => 'Video(iframe)','ui' => $config->ui])

	->addImage('webinar_image', [
		'label' => 'Guest Speaker Image',
		'instructions' => 'Image must be 300 x 300',
		'min_width' => '300px',
	    'min_height' => '300px',
	    'max_width' => '300px',
	    'max_height' => '300px'
	])

	->addTrueFalse('add_ceu', [
		'label' => 'CE Credit?'
	]);
    
return $webinars;