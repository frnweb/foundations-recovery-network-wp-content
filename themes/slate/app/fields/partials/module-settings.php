<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$settings = new FieldsBuilder('module-settings');

$settings
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.grid_options'));

return $settings;