<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsarticles = new FieldsBuilder('articles_options');

$optionsarticles

->setLocation('options_page', '==', 'theme-general-settings')

// Post Relationship Field
	->addRelationship('article', [
	    'label' => 'Article Picker',
        'post_type' => 'post',
        'min' => 5,
		'max' => 5,
	    'ui' => $config->ui,
	    'wrapper' => ['width' => 80]
    ])
    	->setInstructions('Choose 5 articles to display on both the articles page and the category sidebar. The first article in the list is considered the featured.')
    //Ad Block 
	->addImage('ad_block', [
		'ui' => $config->ui,
	])
		->setInstructions('This image will appear as an ad block in the category archive sidebar')
	
	->addUrl('ad_url', [
		'ui' => $config->ui,
	])
		->setInstructions('Link for adblock archive sidebar');
return $optionsarticles;