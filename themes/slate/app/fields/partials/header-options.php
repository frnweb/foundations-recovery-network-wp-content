<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsheader = new FieldsBuilder('header_options');

$optionsheader

->setLocation('options_page', '==', 'theme-header-settings')

	//GTM checkbox
	->addTrueFalse('add_gtm')

	//GTM Tag Manager
	->addText('gtm_code', [
		'label' => 'GTM Code Snippet',
		'ui' => $config->ui
	])
	->conditional('add_gtm', '==', 1)

	//Logo checkbox
	->addTrueFalse('add_logo')

    //Logo 
	->addImage('logo', [
		'label' => 'Logo',
		'ui' => $config->ui
	])
    ->conditional('add_logo', '==', 1);
    
return $optionsheader;