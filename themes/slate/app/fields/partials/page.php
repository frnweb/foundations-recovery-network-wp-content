<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('Modules');

$page
  ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'template-landing.php')
    ->or('page_template', '==', 'template-webinar.php')
  ->setGroupConfig('style', 'seamless');
  
$page
  ->addFields(get_field_partial('partials.builder'));


return $page;