<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$builder = new FieldsBuilder('builder');

$builder
   	->addFlexibleContent('modules', [
   		'button_label' => 'Add Module'
   	])
	    ->addLayout(get_field_partial('modules.articles_podcasts'))
	    ->addLayout(get_field_partial('modules.billboard'))
		->addLayout(get_field_partial('modules.carousel'))
		->addLayout(get_field_partial('modules.conference'))
		->addLayout(get_field_partial('modules.cpt_duo'))
		->addLayout(get_field_partial('modules.deck'))
		->addLayout(get_field_partial('modules.duo'))
		->addLayout(get_field_partial('modules.email'))
		->addLayout(get_field_partial('modules.events_loop'))
		->addLayout(get_field_partial('modules.event_duo'))
		->addLayout(get_field_partial('modules.facility'))
		->addLayout(get_field_partial('modules.facility_duo'))
		->addLayout(get_field_partial('modules.facility_tour'))
		->addLayout(get_field_partial('modules.facility_tabs'))
		->addLayout(get_field_partial('modules.faq'))
		->addLayout(get_field_partial('modules.helpful_links'))
		->addLayout(get_field_partial('modules.insurance'))
		->addLayout(get_field_partial('modules.latest_release'))
		->addLayout(get_field_partial('modules.modal'))
		->addLayout(get_field_partial('modules.news'))
		->addLayout(get_field_partial('modules.post_content'))
		->addLayout(get_field_partial('modules.similar_locations'))
   		->addLayout(get_field_partial('modules.spotlight'))
		->addLayout(get_field_partial('modules.staff'))
		->addLayout(get_field_partial('modules.related_articles'))
		->addLayout(get_field_partial('modules.vertical_carousel'))
		->addLayout(get_field_partial('modules.vob_form'))
		->addLayout(get_field_partial('modules.webinar'))
		->addLayout(get_field_partial('modules.wildcard'))
		->addLayout(get_field_partial('modules.wild_nav'))
		->addLayout(get_field_partial('modules.wysiwyg'))
		->addLayout(get_field_partial('modules.wrapper_open'))
		->addLayout(get_field_partial('modules.wrapper_close'))
		;
return $builder;