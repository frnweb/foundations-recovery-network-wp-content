<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$insurance = new FieldsBuilder('insurance');

$insurance
	->addFields(get_field_partial('partials.module-settings'));

$insurance
	// Type Selector
	->addTab('Type Selecter', ['placement' => 'left'])
		// Select
		->addSelect('type_select', [
			'label' => 'Module Type',
			'wrapper' => ['width' => 30]
		])
		
			->addChoices(
				['duo' => 'Duo'],
				['deck' => 'Deck']
		)
		
	->addTab('content', ['placement' => 'left'])

	//Custom Text
	->addTrueFalse('check_box', [
		'label' => 'Add Custom Title',
	])
	->setInstructions('Optional custom supporting text. Default: Foundations accepts most major insurance providers, including those shown here.')
	
	->addText('insurance_text', [
		'label' => 'Custom Title',
	])
	->conditional('check_box', '==', 1 )

  	->addSelect('row', [
  		'label' => 'Row Count',
  		'wrapper' => ['width' => 25]
	])
	->setInstructions('# of icons on a row')

    ->addChoices(
        ['1' => '1'],
        ['2' => '2'],
        ['3' => '3'],
        ['4' => '4'],
        ['5' => '5'],
        ['6' => '6']
	)
	 
	->addCheckbox('insurance', [
			'layout' => 'vertical',
			'toggle' => 1,
			'wrapper' => ['width' => 75]
	])
		->addChoices(
			['4yourchoice' => '4YourChoice'],
			['aetna' => 'Aetna'],
			['anthem' => 'Anthem'],
			['beaconhealth' => 'BeaconHealth'],
			['bluecalifornia' => 'Blue California'],
			['bluecross' => 'BlueCross'],
			['cigna' => 'Cigna'],
			['compsych' => 'ComPsych'],
			['coresource' => 'CoreSource'],
			['coventry' => 'Coventry'],
			['healthnet' => 'HealthNet'],
			['humana' => 'Humana'],
			['magellan' => 'Magellan'],
			['priorityhealth' => 'PriorityHealth'],
			['tricare' => 'Tricare'],
			['unitedhealthcare' => 'UnitedHealthcare'],
      		['valueoptions' => 'ValueOptions'],
	)
	
	//Button
	->addFields(get_field_partial('modules.button'));
    
return $insurance;