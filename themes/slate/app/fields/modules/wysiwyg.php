<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 50],
];

$wysiwyg = new FieldsBuilder('wysiwyg');

$wysiwyg
	->addFields(get_field_partial('partials.module-settings'));

$wysiwyg
    ->addTab('content', ['placement' => 'left'])
    // WYSIWYG
    ->addWysiwyg('content')
    
    //Map
    ->addFields(get_field_partial('modules.google_map'));

return $wysiwyg;