<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$staff = new FieldsBuilder('staff');

$staff
	->addFields(get_field_partial('partials.module-settings'));

$staff
	->addTab('content', ['placement' => 'left'])

	// Header
	->addTrueFalse('check_box', [
			'label' => 'Add Header',
			'wrapper' => ['width' => 30]
		])
		->setInstructions('Optional header for the wrapper')
		
		->addText('header', [
			'label' => 'Header',
			'wrapper' => ['width' => 70]
		])
		->conditional('check_box', '==', 1 )
	// Type Select
	->addSelect('type_select', [
		'label' => 'Staff Display Select',
		'ui' => $config->ui,
	])
		->addChoices('deck', 'spotlight')

	//Staff Deck
    ->addRelationship('staff', [
        'label' => 'Staff Members',
        'post_type' => 'sl_staff_cpts',
        'min' => 1,
    ])
    ->setInstructions('Choose multiple staff members to highlight');
    

return $staff;