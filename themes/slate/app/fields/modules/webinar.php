<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
    'ui' => 1,
    'wrapper' => ['width' => 100],
];

$webinar = new FieldsBuilder('webinar');

$webinar
    ->addFields(get_field_partial('partials.module-settings'));

$webinar
    ->addTab('content', ['placement' => 'left'])
    
    ->addText('header', [
        'label' => 'Header'
    ])

    // Post Relationship Field
    ->addRelationship('webinars', [
        'label' => 'Featured Webinar',
        'post_type' => 'sl_webinars_cpts',
        'min' => 1,
        'max' => 1,
        'ui' => $config->ui,
        'wrapper' => ['width' => 80]
    ]);
    

return $webinar;