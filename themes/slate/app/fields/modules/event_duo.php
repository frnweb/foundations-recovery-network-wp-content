<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$event_duo = new FieldsBuilder('event_duo');

$event_duo
	->addFields(get_field_partial('partials.module-settings'));

$event_duo
  	->addTab('content', ['placement' => 'left'])
		// Paragraph
		->addWysiwyg('wysiwyg', [
	        'label' => 'Wysiwyg'
	    ])
			->setInstructions('Wysiwyg for the duo')

		//Video CTA
		->addTrueFalse('add_cta', [
			'label' => 'Customize Video CTA',
			'wrapper' => ['width' => 30]
		])
		->setInstructions('The default text is "Last Years Recap" ')

			// Video
	    	->addText('video_cta', [
	    		'label' => 'Video CTA',
	    		'wrapper' => ['width' => 70]
	    	])
    	->conditional('add_cta', '==', 1 );

return $event_duo;