<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$news = new FieldsBuilder('news');

$news
	->addFields(get_field_partial('partials.module-settings'));

$news
	->addTab('content', ['placement' => 'left'])

		//Repeater
		->addRepeater('news', [
		  'min' => 3,
		  'max' => 9,
		  'button_label' => 'Add Article',
		  'layout' => 'block',
		])
			//Published Date
			->addDatePicker('published_date', [
				'label' => 'Published Date',
				'display_format' => 'm/d/Y',
				'return_format' => 'n.j.y',
				'wrapper' => ['width' => 20]
			])  
			//Article URL
			->addURL('url', [
				'label' => 'Article URL',
				'wrapper' => ['width' => 80]
			])
			//Article Title
			->addtext('title', [
				'label' => 'Article Title',
				'wrapper' => ['width' => 50]
			])
			//Media Source
			->addtext('source', [
				'label' => 'Media Source',
				'wrapper' => ['width' => 50]
			]);

return $news;