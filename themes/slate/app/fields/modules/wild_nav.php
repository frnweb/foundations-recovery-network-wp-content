<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 50],
];

$wild_nav = new FieldsBuilder('wild_nav');

$wild_nav
    ->addFields(get_field_partial('partials.module-settings'));


$wild_nav
    ->addTab('content', ['placement' => 'left'])
    //Optional Intro
    ->addTrueFalse('intro_check', [
        'label' => 'Intro?',
        'wrapper' => ['width' => 15]
    ])
        ->setInstructions('Optional intro for the module')
        ->addWysiwyg ('module_intro', [
            'wrapper' => ['width' => 85],
            'new_lines' => 'wpautop'
        ])
    ->conditional('intro_check', '==', 1)
    //Repeater
    ->addRepeater('items', [
      'min' => 1,
      'max' => 10,
      'button_label' => 'Add Item',
      'layout' => 'block',
    ])
        //Item Header
        ->addText('header')

        //Item Menu Title
        ->addText('menu_title')

        //Flexible Content
        ->addFlexibleContent('pieces', ['button_label' => 'Add Pieces'])
            // CALLOUT
            ->addLayout('callout')
                //Optional Deck Class
                ->addTrueFalse('add_class', [
                'wrapper' => ['width' => 15]
                ])
                    ->addText('callout_class', [
                        'wrapper' => ['width' => 85]
                    ])
                ->conditional('add_class', '==', 1)
                ->addWysiwyg('callout_wysiwyg', [
                    'label' => 'Wysiwyg',
                    'new_lines' => 'wpautop'
                ])
            // DECK
            ->addLayout('deck')
                //Optional Deck Class
                ->addTrueFalse('add_class', [
                    'wrapper' => ['width' => 15]
                ])
                    ->addText('deck_class', [
                        'wrapper' => ['width' => 85]
                    ])
                 ->conditional('add_class', '==', 1)
                //Repeater
                ->addRepeater('deck', [
                  'min' => 1,
                  'max' => 10,
                  'button_label' => 'Add Card',
                  'layout' => 'block',
                ])
                // Image 
                ->addImage('deck_image')  
                // Header
                ->addText('deck_header')
                // Paragraph
                ->addTextArea('deck_text')
                // Button 
                ->addFields(get_field_partial('modules.button'))
            // VIDEO
            ->addLayout('video')
                ->addImage('video_image')
                ->addText('video_cta', [
                    'label' => 'Video CTA',
                ])
                // Video
                ->addWysiwyg('video', [
                    'label' => 'Video(iframe)',
                ])
            // WYSIWYG
            ->addLayout('wysiwyg')
                ->addWysiwyg('wysiwyg', [
                    'new_lines' => 'wpautop'
                ])
            // WRAPPER
            ->addLayout('wrapper_open')
                //Optional Deck Class
                ->addTrueFalse('add_class', [
                    'wrapper' => ['width' => 15]
                ])
                    ->addText('wrapper_class', [
                        'wrapper' => ['width' => 85]
                    ])
                 ->conditional('add_class', '==', 1)
            ->addLayout('wrapper_close');

return $wild_nav;