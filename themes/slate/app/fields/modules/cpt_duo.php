<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$cpt_duo = new FieldsBuilder('cpt_duo');

$cpt_duo
	->addFields(get_field_partial('partials.module-settings'))
    //Optional Module Header
    ->addTrueFalse('check_box', [
        'label' => 'Add Module Header',
        'wrapper' => ['width' => 20]
    ])
    ->addText('module_header', [
        'label' => 'Module Header',
        'wrapper' => ['width' => 80]
    ])
    ->conditional('check_box', '==', 1 );

$cpt_duo
	->addTab('custom_post_type', ['placement' => 'left'])
		->addSelect('type_select', [
			'label' => 'Custom Post Type',
      'ui' => $config->ui,
		])
	  	->addChoices(
		  ['podcast' => 'Podcast'],
          ['webinar' => 'Webinar'],
          ['conferences' => 'Conferences']
        )
          
  	->addTab('content', ['placement' => 'left'])
		// Card
		->addFields(get_field_partial('modules.card'));
		
return $cpt_duo;