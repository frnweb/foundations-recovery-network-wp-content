<?php
/**
 * Setup file used to register theme features.
 */



/**
 * Register Slate Menus.
 */

function sl_register_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
	  'custom-menu' => __( 'Custom Menu' ),
	  'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'sl_register_menus' );


/**
 * Register Slate Sidebar.
 */
function sl_widgets_sidebar_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'side_1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'sl_widgets_sidebar_init' );

/**
 * Register ACF Theme Options Page.
 */
if( function_exists('acf_add_options_page') ) {
	
    acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
		'menu_slug'		=> 'theme-header-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
		'menu_slug'		=> 'theme-footer-settings',
	));
	
}

/**
 * Slate Admin Styles
 */



